var TokenProvider = require('..');
var assert = require('assert');
var EventEmitter = require('events').EventEmitter;
var testData = require('./encryptionTest');
var crypto = require('origami-crypto-utils');

function StubTokenProvider (
  fold,
  constructorError,
  constructorStringError,
  methodError,
  methodStringError) {
  this.fold = fold;
  this.methodError = methodError;
  this.methodStringError = methodStringError;

  if (constructorStringError) throw constructorStringError;
  if (constructorError) throw new Error('my constructor error');
}

StubTokenProvider.prototype.getToken = function (username, password) {
  if (this.methodError) throw new Error('my method error');
  if (this.methodStringError) throw this.methodStringError;

  if (!this.fold) return Promise.reject('fold is required');

  if (username === 'me' && this.fold === 'fold1' && password === 'pass') {
    return Promise.resolve(
      {
        permissions: 'some'
      }
    );
  }
};

StubTokenProvider.prototype.getToken2 = function (username, password) {
  return {
    permissions: 'some'
  };
};


describe('TokenProvider', function () {
  describe('constructor', function () {
    it('requires an API', function () {
      assert
        .throws(
          function () {
            new TokenProvider();
          },
          /API is required to be a function/
        )
    });

    it('requires a private key', function () {
      assert
        .throws(
          function () {
            new TokenProvider(
              StubTokenProvider,
              null
            );
          },
          /Private key is required/
        )
    });

    it('rejects invalid private key', function () {
      assert
        .throws(
          function () {
            new TokenProvider(
              StubTokenProvider,
              'invalid'
            );
          },
          /Private key is invalid/
        )
    });

    it('instantiates', function () {
      var target = new TokenProvider(
        StubTokenProvider,
        testData.tokenProvider.privateKey
      );

      assert(target);
    });
  });

  describe('.createInstance', function () {
    it('creates an instance', function () {
      var target = new TokenProvider(
        StubTokenProvider,
        testData.tokenProvider.privateKey
      );

      var instance = target.createInstance(
        {
          fold: 'fold1'
        }
      );

      assert('fold1', instance.fold1);
    });
  });

  describe('.invokeMethod', function () {
    it('invokes method', function (done) {
      var target = new TokenProvider(
        StubTokenProvider,
        testData.tokenProvider.privateKey
      );

      target
      .invokeMethod(
        {
          fold: 'fold1'
        },
        'getToken',
        {
          username: 'me',
          password: 'pass'
        }
      )
      .then(
        function (result) {
          try {
            assert.deepEqual(
              {
                permissions: 'some'
              },
              result
            );

            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
  });
});
