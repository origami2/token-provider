var fnHelpers = require('origami-js-function-helpers');
var crypto = require('origami-crypto-utils');

function TokenProvider(api, privateKey) {
  if (typeof(api) !== 'function') throw new Error('API is required to be a function');
  if (!privateKey) throw new Error('Private key is required');

  this.api = api;
  this.apiName = api.name;
  this.methods = {};

  for (var methodName in api.prototype) {
    this.methods[methodName] = fnHelpers.getFunctionArgumentNames(api.prototype[methodName]);
  }

  try {
    this.privateKey = crypto.asPrivateKey(privateKey);
  } catch (e) {
    throw new Error('Private key is invalid');
  }
}

TokenProvider.prototype.createInstance = function (context) {
  var args = fnHelpers
    .getFunctionArgumentNames(this.api)
    .map(function (argName) {
      return context[argName];
    });

  var target = Object.create(this.api);

  this.api.apply(target, args);

  return target;
};

TokenProvider.prototype.invokeMethod = function (context, methodName, params) {
  var instance = this.createInstance(context);
  var method = this.api.prototype[methodName];

  var args = fnHelpers
    .getFunctionArgumentNames(method)
    .map(function (argName) {
      return params[argName];
    });

  return method.apply(instance, args);
};

module.exports = TokenProvider;
